import org.kde.kirigami 2.7 as Kirigami

// About Dialog
Kirigami.AboutPage {
	aboutData: {
		"displayName": "LLs VideoPlayer",
		"productName": "vplayer",
		"shortDescription": "Simple video player for Plasma Mobile",
		"homepage": "https://invent.kde.org/jbbgameich/videoplayer",
		"version": "0.3",
		"desktopFileName": "org.kde.mobile.vplayer",
		"programIconName": "org.kde.mobile.vplayer",
		"authors": [
			{
				"name": "Leszek Lesner",
				"emailAddress": "leszek@zevenos.com",
			},
			{
				"name": "Jonah Brüchert",
				"emailAddress": "jbb@kaidan.im"
			}
		],
		"licenses" : [
			{
				"name": "GPL v2",
				"spdx": "GPL-2.0"
			}
		],
		"copyrightStatement": "© 2015-2019 LLs VideoPlayer developers and contributors",
		"desktopFileName": "org.kde.mobile.vplayer"
	}
}
