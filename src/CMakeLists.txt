set(vplayer_SRCS
    main.cpp
    )

qt5_add_resources(RESOURCES resources.qrc)

add_executable(vplayer ${vplayer_SRCS} ${RESOURCES})
target_link_libraries(vplayer Qt5::Core Qt5::Qml Qt5::Quick Qt5::Widgets)

install(TARGETS vplayer ${KF5_INSTALL_TARGETS_DEFAULT_ARGS})
